/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import 'react-native-gesture-handler';
import {
	NavigationContainer,
	NavigationProp,
	RouteProp,
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {SafeAreaProvider} from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import {Alert, Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export type RootStackParamList = {
	Exercise3: undefined;
	Exercise4: undefined;
};

type Exercise3Props = {
	navigation: NavigationProp<RootStackParamList, any>;
};

const Exercise3: React.FC<Exercise3Props> = ({navigation}) => {
	return (
		<React.Fragment>
			<View style={styles.outer}>
				<View
					style={{
						alignSelf: 'center',
						position: 'absolute',
					}}>
					<TouchableOpacity
						style={{
							width: 120,
							height: 40,
							borderWidth: 1,
							borderColor: '#3d3d3d',
							borderRadius: 18,
							justifyContent: 'center',
							alignItems: 'center',
						}}
						onPress={() => navigation.navigate('Exercise4')}>
						<Text>Exercise4</Text>
					</TouchableOpacity>
				</View>
				<View style={styles.inner}>
					<View style={[styles.box, {backgroundColor: 'red'}]} />
					<View style={[styles.box, {backgroundColor: 'lime'}]} />
				</View>
				<View style={[styles.inner, {alignItems: 'flex-end'}]}>
					<View style={[styles.box, {backgroundColor: 'blue'}]} />
					<View style={[styles.box, {backgroundColor: 'pink'}]} />
				</View>
			</View>
		</React.Fragment>
	);
};

const HomeworkData: {title: string; content: string}[] = [
	{
		title: 'React Course',
		content: 'Course about how to write the React Web framework',
	},
	{
		title: 'React Native Course',
		content:
			'Course about how to write the Mobile App in IOS and Android by using React-Native',
	},
	{
		title: 'React Course',
		content:
			'Course about how to write the Mobile App in IOS and Android by using React-Native',
	},
];

const Exercise4: React.FC = () => {
	return (
		<React.Fragment>
			<View style={styles.container}>
				{HomeworkData.map((item) => (
					<View style={styles.row}>
						<Image
							style={styles.itemImage}
							source={{
								uri:
									'https://source.unsplash.com/random/200x300',
							}}
						/>
						<View style={styles.description}>
							<Text style={styles.header}>{item.title}</Text>
							<Text ellipsizeMode="tail">{item.content}</Text>
						</View>
					</View>
				))}
			</View>
		</React.Fragment>
	);
};

const styles = StyleSheet.create({
	outer: {
		flex: 1,
	},
	inner: {
		flex: 1,
		justifyContent: 'space-between',
		flexDirection: 'row',
	},
	box: {
		width: 50,
		height: 50,
	},
	container: {
		flex: 1,
	},
	row: {
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: 5,
	},
	header: {
		fontSize: 20,
	},
	itemImage: {
		width: 100,
		height: 100,
		// backgroundColor: '#000',
	},
	description: {
		flexShrink: 1,
		paddingLeft: 5,
		alignSelf: 'center',
	},
});

const RootStack = createStackNavigator<RootStackParamList>();

export default () => {
	const [isLoading, setIsLoading] = React.useState(true);

	React.useEffect(() => {
		setTimeout(() => {
			setIsLoading(false);
		}, 3000);
	}, [isLoading]);

	if (isLoading) {
		return (
			<LottieView
				source={require('./assets/lotties/8205-loading-animation.json')}
				autoPlay
				loop
			/>
		);
	}
	return (
		<React.Fragment>
			<SafeAreaProvider>
				<NavigationContainer>
					<RootStack.Navigator
						screenOptions={{headerTitleAlign: 'center'}}>
						<RootStack.Screen
							name="Exercise3"
							component={Exercise3}
						/>
						<RootStack.Screen
							name="Exercise4"
							component={Exercise4}
						/>
					</RootStack.Navigator>
				</NavigationContainer>
			</SafeAreaProvider>
		</React.Fragment>
	);
};
