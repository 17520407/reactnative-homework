module.exports = {
	presets: ['module:metro-react-native-babel-preset'],
	plugins: [
		['import', {libraryName: '@ant-design/react-native'}], // The difference with the Web platform is that you do not need to set the style
		[
			'module-resolver',
			{
				root: ['./src'],
				extensions: [
					'.ios.js',
					'.android.js',
					'.js',
					'.ts',
					'.tsx',
					'.json',
				],
				alias: {
					tests: ['./tests/'],
					'@components': './src/components',
				},
			},
		],
	],
};
